$(document).ready(function() {

    $('#travelform-travelfrom').kladr({
        type: $.kladr.type.city
    });

    $('#travelform-travelto').kladr({
        type: $.kladr.type.city
    });

    $('button[data-type=toggle-activity]').on( 'click', function() {
        $.ajax({
            url : '/driver/toggle-activity?id=' + $(this).data('id'),
            success : function(data) {
                if (data.success === true) {
                    if(data.data.active) {
                        $('#container_' + data.data.id).removeClass('panel-default').addClass('panel-success');
                        $('span[data-id=' + data.data.id + ']').html('Активен');
                    } else {
                        $('#container_' + data.data.id).removeClass('panel-success').addClass('panel-default');
                        $('span[data-id=' + data.data.id + ']').html('Не активен');
                    }

                } else {
                    alert("Произошла ошибка при выполнении изменений:\n" + data.message )
                }
            },
            fail: function() {
                alert('Произошла ошибка сервера. Обратитесь к администратору.');
            }
        });
    });

});
