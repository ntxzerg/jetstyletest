<?php
/**
 * Created by PhpStorm.
 * User: Siniachevsky Constantine
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author ntXine <ntxine@gmail.com>
 */

class KladrAsset extends AssetBundle
{
	public $sourcePath = '@vendor/bower/jquery.kladr';
	public $css = [
		'jquery.kladr.min.css',
	];
	public $js = [
		'jquery.kladr.min.js',
	];
}
