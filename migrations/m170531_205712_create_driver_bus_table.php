<?php

use yii\db\Migration;

/**
 * Таблица связей водителей и автобусов (junction table many-to-many).
 */
class m170531_205712_create_driver_bus_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('driver_bus', [
            'id' => $this->primaryKey(),
            'driver_id' => $this->integer(),
            'bus_id' => $this->integer(),
        ]);

	    $this->addForeignKey(
		    'fk-driver_bus-driver_id',
		    'driver_bus',
		    'driver_id',
		    'drivers',
		    'id',
		    'RESTRICT'
	    );

	    $this->addForeignKey(
		    'fk-driver_bus-bus_id',
		    'driver_bus',
		    'bus_id',
		    'buses',
		    'id',
		    'RESTRICT'
	    );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('driver_bus');
    }
}
