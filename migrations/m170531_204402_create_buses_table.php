<?php

use yii\db\Migration;

/**
 * Таблица автобусов (модель Bus)
 */
class m170531_204402_create_buses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('buses', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(),
            'avg_speed'     => $this->integer(),
            'created_at'    => $this->dateTime()->notNull(),
            'updated_at'    => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('buses');
    }
}
