<?php

use yii\db\Migration;

/**
 * Таблица водителей (модель Driver).
 */
class m170531_204049_create_drivers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('drivers', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(),
            'birth_date'    => $this->dateTime(),
            'active'        => $this->boolean(),
            'photo'         => $this->string(),
            'created_at'    => $this->dateTime()->notNull(),
            'updated_at'    => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('drivers');
    }
}
