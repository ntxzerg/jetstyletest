<?php
namespace app\commands;

use yii\console\Controller;
use \app\rbac\UserGroupRule;


/**
 * Class RbacController Инициализация модели доступа данными по-умолчанию
 * @package app\commands
 */
class RbacController extends Controller
{
	/**
	 *  Метод инициализации
	 */
	public function actionInit()
	{
		$authManager = \Yii::$app->authManager;

		/**
		 * Создаём необходимые роли
		 */
		$guest  = $authManager->createRole('guest');
		$editor  = $authManager->createRole('editor');
		$admin  = $authManager->createRole('admin');

		/**
		 * Создаём необходимые разрешения
		 */
		$login  = $authManager->createPermission('login');
		$logout = $authManager->createPermission('logout');
		$error  = $authManager->createPermission('error');
		$index  = $authManager->createPermission('index');
		$view   = $authManager->createPermission('view');
		$update = $authManager->createPermission('update');
		$delete = $authManager->createPermission('delete');

		/** Добавляем разрешения в менеджер */
		$authManager->add($login);
		$authManager->add($logout);
		$authManager->add($error);
		$authManager->add($index);
		$authManager->add($view);
		$authManager->add($update);
		$authManager->add($delete);

		/** Создаём новое правило */
		$userGroupRule = new UserGroupRule();
		$authManager->add($userGroupRule);

		/** Добавляем правило в роли */
		$guest->ruleName  = $userGroupRule->name;
		$editor->ruleName  = $userGroupRule->name;
		$admin->ruleName  = $userGroupRule->name;

		/** Добавляем правило в менеджер */
		$authManager->add($guest);
		$authManager->add($editor);
		$authManager->add($admin);

		// Гость
		$authManager->addChild($guest, $login);
		$authManager->addChild($guest, $logout);
		$authManager->addChild($guest, $error);
		$authManager->addChild($guest, $index);
		$authManager->addChild($guest, $view);

		// Редактор
		$authManager->addChild($editor, $update);
		$authManager->addChild($editor, $guest);


		// Админ
		$authManager->addChild($admin, $delete);
		$authManager->addChild($admin, $editor);
	}
}