<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driver-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Фамилия Имя Отчество') ?>

	<?= $form->field($model, 'birth_date')->widget(
		\dosamigos\datepicker\DatePicker::className(), [
        'language' => 'ru',
		'inline' => true,
		'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
		'clientOptions' => [
			'autoclose' => true,
			'format' => 'yyyy-mm-dd'
		]
	]);?>

	<?= $form->field($model, 'active')->checkbox([ 'label' => 'Активен', 'disabled' => false ]) ?>

	<?= $form->field($model, 'photo')->fileInput()->label('Фотография') ?>
    <div>
        <p>
            * Если файл с фотографией не выбран, то сохранится предыдущий файл для изменяемой записи.
            Для новой записи в таком случае файл не будет загружен.
        </p>
    </div>

	<?= $form->field($model, 'buses_ids')->dropDownList(\app\models\Bus::listAsArray(),['multiple' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
