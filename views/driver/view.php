<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
/* @var $canEdit bool */
/* @var $canDelete bool */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Водители', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($canEdit) { ?>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php } ?>

        <?php if ($canDelete) { ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить запись?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
	        'photo:image',
            'name',
            'birth_date:date',
            'active:boolean',
            'created_at:datetime:Создан',
            'updated_at:datetime:Последнее обновление',
        ],
    ]) ?>

</div>
