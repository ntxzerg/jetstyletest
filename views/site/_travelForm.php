<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TravelForm */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="panel panel-warning travel-params">
    <div class="panel-body">

	    <?php $form = ActiveForm::begin([
	            'action' => \yii\helpers\Url::current(),
                'method' => 'post',]); ?>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		    <?= $form->field($model, 'travelFrom')->textInput() ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	        <?= $form->field($model, 'travelTo')->textInput() ?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
	        <?= Html::submitButton('Расчитать', ['class' => 'btn btn-success']) ?>
        </div>

	    <?php ActiveForm::end(); ?>

    </div>
</div>
