<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О задании';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <h2>Автобусный парк</h2>
    <p>
        Требуется реализовать возможность администрирования водителей автопарка на Yii2 PHP Framework с БД MySQL. Обязательно использование миграций и phpDoc.
    </p>
</div>
