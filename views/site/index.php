<?php

/* @var $this yii\web\View */
/* @var $travel \app\models\TravelForm */
/* @var $searchModel app\models\DriverSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $canAdd bool */
/* @var $canEdit bool */
/* @var $canDelete bool */
/* @var $travelFrom string */
/* @var $travelTo string */

$this->title = 'JetStyle тестовое задание';
$dataProvider->prepare();
?>

<div class="site-index">
    <div class="body-content centered">

	    <?= $this->render('_travelForm', ['model' => $travel,]);?>

        <div class="me-flex-container">
            <?php /** @var \app\models\Driver $model */?>
            <?php foreach ($dataProvider->getModels() as $model) { ?>
                <?php $panelClass = $model->active ? 'panel-success' : 'panel-default' ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 me-flex-item">
                    <div class="panel <?= $panelClass ?>" id="container_<?= $model->id ?>">
                        <div class="panel-heading">
                            <div class="panel-title">
	                            <?= \yii\helpers\Html::a(
	                                    $model->name,
                                        ['driver/view', 'id' => $model->id],
                                        ['class' => 'col-lg-12 col-md-12 col-sm-12 col-xs-12'])
                                ?>
                            </div>
                        </div>
                        </a>
                        <div class="panel-body top">
                            <div class="col-md-6 col-xs-6">
                                <p>
                                    Возраст:
                                    <?= $model->age ?>
	                                <?= Yii::t('app',
	                                           '{age, plural, one{год} =2{года} =3{года} =4{года} other{лет}}',
	                                           ['age' => $model->age%10 ]) ?>
                                </p>
                                <?php if ($travel->distance > 0) { ?>
                                    <p>
                                        В пути:
                                        <?php $days = $model->getMinDaysByDistance($travel->distance); ?>
                                        <?= $days ?>
                                        <?= Yii::t('app',
                                            '{days, plural, one{день} =2{дня} =3{дня} =4{дня} other{дней}}',
                                            ['days' => $days%10 ]) ?>
                                    </p>
                                <?php } ?>

                            </div>
                            <div class="col-md-6 col-xs-6">
                                <img class="img-rounded" width="100%"
                                     src="<?= (empty($model->photo)
                                         || !file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$model->photo))
                                         ? '/upload/blank-person.png'
                                         : $model->photo?>">
                            </div>
                        </div>
                        <div class="panel-body bottom">
                            Модели автобусов:
                            <ul class="list-group">
                                <?php foreach ($model->buses as $bus) { ?>
                                    <li class="list-group-item" data-id="<?= $bus->id ?>" data-name="bus">
                                        <?= $bus->name ?>
                                        <span class="pull-right"></span>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="panel-footer">
                            <span>
                                <span data-id="<?= $model->id ?>">
                                    <?= ($model->active) ? 'Активен' : 'Не активен' ?>
                                </span>
                                <?php if ($canEdit) { ?>
                                    <button
                                        type="button"
                                        class="btn btn-default pull-right"
                                        title="Активировать/деактивировать"
                                        data-type="toggle-activity"
                                        data-id="<?= $model->id ?>">
                                        <div class="glyphicon glyphicon-refresh" aria-hidden="true"></div>
                                    </button>
                                <?php } ?>
                            </span>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-12 centered">
            <?php echo \yii\widgets\LinkPager::widget(['pagination' => $dataProvider->pagination,]); ?>
        </div>
    </div>
</div>
