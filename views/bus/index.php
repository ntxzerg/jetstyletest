<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $canAdd bool */
/* @var $canEdit bool */
/* @var $canDelete bool */

$this->title = 'Автобусы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="driver-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?php if ($canAdd) { ?>
        <p>
			<?= Html::a('Добавить автобус', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
	<?php } ?>

<!--	--><?php //Yii::$app->formatter->locale = 'ru-RU' ?>
	<?= GridView::widget([
         'dataProvider' => $dataProvider,
         'filterModel' => $searchModel,
         'layout'=>"{pager} {summary} {items} {pager}",
         'columns' => [
             ['class' => 'yii\grid\SerialColumn'],

             'name:text',
             'avg_speed:text',
             [
                 'class' => 'yii\grid\ActionColumn',
                 'template' => '{view} {update} {delete}',
                 'buttons' => [
                     'update' => $canEdit
                         ? function($url) {
                             return Html::a(
                                 '<span class="glyphicon glyphicon-pencil"></span>',
                                 $url);
                         }
                         : function() {
                             return '';
                         },
                     'delete' => $canDelete
                         ? function($url) {
                             return Html::a(
                                 '<span class="glyphicon glyphicon-trash"></span>',
                                 $url);
                         }
                         : function() {
                             return '';
                         },
                 ]
             ],
         ],
     ]); ?>
</div>
