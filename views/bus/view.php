<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Bus */
/* @var $canEdit bool */
/* @var $canDelete bool */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Автобусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	    <?php if ($canEdit) { ?>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	    <?php } ?>

	    <?php if ($canDelete) { ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ]) ?>
	    <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'avg_speed',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
