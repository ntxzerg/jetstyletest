<?php
namespace app\rbac;

use app\models\User;
use yii\rbac\Rule;

class UserGroupRule extends Rule
{
	public $name = 'userGroup';
	public $_assignments = [];

	public function execute($user, $item, $params)
	{
		if ($role = $this->userRole($user)) {
			switch ($item->name) {
				case 'admin':
					return $role == 'admin';

				case 'editor':
					return $role == 'admin' || $role == 'editor';

				case 'guest':
					return in_array($role, ['admin', 'editor', 'guest']);
			}
		}
		return false;
	}

	/**
	 * @param integer|null $userId ID of user.
	 * @return string|false
	 */
	protected function userRole($userId)
	{
		$user = \Yii::$app->user;
		if ($userId === null) {
			if ($user->isGuest) {
				return 'guest';
			}
			return false;
		}
		if (!isset($this->_assignments[$userId])) {
			$role = false;
			if (!$user->isGuest && $user->id == $userId) {
				$role = User::getUserRole($userId);
//				$role = $user->role;
			} elseif ($user->isGuest || $user->id != $userId) {
				$role = User::getUserRole($userId);
			}
			$this->_assignments[$userId] = $role;
		}
		return $this->_assignments[$userId];
	}
}