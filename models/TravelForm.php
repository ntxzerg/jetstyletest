<?php
/**
 * Created by PhpStorm.
 * User: Siniachevsky Constantine
 * Date: 04.06.17
 * Time: 23:08
 */

namespace app\models;


use yii\base\Model;

/**
 * Класс для формы путешествий
 *
 * @package app\models
 */
class TravelForm extends Model
{
	/**
	 * @var
	 */
	public $travelFrom;
	public $travelTo;
	public $distance;

	/**
	 * @return array Массив правил валидации.
	 */
	public function rules() : array
	{
		return [
			['travelFrom',  'required', 'message' => 'Выберите пункт отправления'],
			['travelTo',    'required', 'message' => 'Выберите пункт назначения'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() : array
	{
		return [
			'travelFrom' => 'Начальный пункт',
			'travelTo' => 'Конечный пункт',
		];
	}

	/**
	 * Расчёт расстояния между двумя городами, если они заданы в модели
	 *
	 * @return int расстояние в километрах
	 */
	public function calculateDistance() : int
	{
		$this->distance = 0;
		if ($this->validate()) {
			$route = json_decode(
				file_get_contents(
					\Yii::$app->params['googleApi_DistanceMatrix'] .
					'&origins=' . urlencode( $this->travelFrom ).
					'&destinations=' . urlencode( $this->travelTo ).
					'&key=' . \Yii::$app->params['googleApi_Key']
				),
				true
			);
			if( $route['status'] == 'OK' && isset($route['rows'][0]['elements'][0]['distance']['value'])) {
				$this->distance = $route['rows'][0]['elements'][0]['distance']['value'] / 1000;
			}
		}

		return $this->distance;
	}

}