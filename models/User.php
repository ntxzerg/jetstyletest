<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
	/**
	 * @var
	 */
	public $id;
	/**
	 * @var
	 */
	public $username;
	/**
	 * @var
	 */
	public $password;
	/**
	 * @var
	 */
	public $role;
	/**
	 * @var
	 */
	public $authKey;
	/**
	 * @var
	 */
	public $accessToken;

	/**
	 * @var array
	 */
	private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'role'      => 'editor',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'admin2',
            'password' => 'admin2',
            'role'      => 'admin',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

	/**
	 * Возвращает набор доступных действий пользователя
	 *
	 * @return array Набор действий для отображения в шаблоне
	 */
//	public static function getActions()
//	{
//		$actions = ['local' => [], 'global' => []];
//
//		if (\Yii::$app->user->can('delete')) {
//			$actions = [
//				'local' => [
//					[
//						'title' => 'Удалить',
//						'class' => 'btn-danger',
//						'data-action' => 'remove',
//						'icon' => 'glyphicon-trash',
//					],
//	            ],
//	        ];
//		}
//
//		if (\Yii::$app->user->can('update')) {
//			$actions['local'][] = [
//				'title' => 'Редактировать',
//				'class' => 'btn-info',
//				'data-action' => 'edit',
//				'icon' => 'glyphicon-pencil',
//			];
//			$actions['local'][] =[
//				'title' => 'Активировать/Деактивировать',
//				'class' => 'btn-warning',
//				'data-action' => 'activate',
//				'icon' => 'glyphicon-refresh',
//			];
//			$actions['global'][] = [
//				'title' => 'Добавить запись',
//				'class' => 'btn-success',
//				'data-action' => 'insert',
//				'icon' => 'glyphicon-plus',
//			];
//		}
//
//		return $actions;
//	}

	/**
	 * @param $userId
	 *
	 * @return mixed
	 */
	public static function getUserRole( $userId)
	{
		return self::$users[$userId]['role'];
	}
}
