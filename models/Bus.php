<?php

namespace app\models;

use voskobovich\linker\LinkerBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Класс модели Автобус (Bus).
 *
 * @property integer $id
 * @property string $name
 * @property integer $avg_speed
 * @property string $created_at
 * @property string $updated_at
 */
class Bus extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['avg_speed'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['drivers_ids'], 'each', 'rule' => ['integer']]
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::class => [
				'class' => TimestampBehavior::class,
				'value' => function () {
					return date('Y-m-d H:i:s');
				}
			],
			[
				'class' => LinkerBehavior::class,
				'relations' => [
					'drivers_ids' => 'drivers',
				],
			],
		];
	}

	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'avg_speed' => 'Средняя скорость (км/ч)',
            'created_at' => 'Запись создана',
            'updated_at' => 'Запись обновлена',
        ];
    }

	/**
	 * Получить список водителей
	 *
	 * @return ActiveQuery
	 */
	public function getDrivers() : ActiveQuery
	{
		return $this->hasMany(
			Bus::className(),
			['id' => 'driver_id']
		)->viaTable(
			'{{%driver_bus}}',
			['bus_id' => 'id']
		);
	}

	/**
	 * Возвращает массив автобусов
	 *
	 * @param array $ids Массив идентификаторов автобусов, ограничивающий выборку
	 *
	 * @return array массив автобусов, где array[bus->id] = bus->name
	 */
	public static function listAsArray(array $ids = [] ) : array
	{
		$buses = empty($ids) ? self::find()->all() : self::findAll(['id' => $ids]);

		$busList = [];

		/** @var Bus $bus */
		foreach ($buses as $bus) {
			$busList[$bus->id] = $bus->name;
		}

		return $busList;
    }

	/**
	 * Поиск модели, возвращает BusQuery для возможности применения условия fastest
	 *
	 * @return BusQuery
	 */
	public static function find() : BusQuery
	{
		return new BusQuery(get_called_class());
    }
}
