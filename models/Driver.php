<?php

namespace app\models;

use tpmanc\imagick\Imagick;
use voskobovich\linker\LinkerBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * Класс модели Водитель (Driver).
 *
 * @property integer $id идентификатор
 * @property string $name Фамилия Имя Отчество
 * @property string $birth_date Дата рождения
 * @property integer $active Активен
 * @property string $photo Фотография
 * @property string $created_at Дата создания записи
 * @property string $updated_at Дата изменения записи
 *
 * @property ActiveQuery $buses Автобусы водителя
 * @property array $buses_ids Идентификаторы автобусов водителя для LinkBehavior
 * @property int $age Вычисляемый возраст по дате рождения
 * @property string $bus_list_string строка с наименованиями автобусов и их идентификаторами для GridView
 * @property Bus $fastest Самый быстрый автобус данного водителя
 * @property int getDaysByDistance Возвращает количество дней для прохождения расстояния
 */
class Driver extends ActiveRecord
{
	/** int MAX_WORK_HOURS максимальное количество рабочих часов */
	const MAX_WORK_HOURS = 8;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birth_date', 'created_at', 'updated_at'], 'safe'],
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['buses_ids'], 'each', 'rule' => ['integer']]
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::class => [
				'class' => TimestampBehavior::class,
				'value' => function () {
					return date('Y-m-d H:i:s');
				}
			],
			[
				'class' => LinkerBehavior::class,
				'relations' => [
					'buses_ids' => 'buses',
				],
			],
		];
	}

	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'birth_date' => 'Дата рождения',
            'active' => 'Активен',
            'photo' => 'Фото',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'buses_ids' => 'Автобусы',
            'busList' => 'Список автобусов'
        ];
    }

	/**
	 * Получить возраст водителя
	 *
	 * @return int age
	 */
	public function getAge() : int
	{
		$birthday_timestamp = strtotime($this->birth_date);
		$age = date('Y') - date('Y', $birthday_timestamp);
		return date('md', $birthday_timestamp) > date('md')
			? --$age
			: $age;
    }

	/**
	 * Получить список автобусов
	 *
	 * @return ActiveQuery
	 */
	public function getBuses() : ActiveQuery
	{
		return $this->hasMany(
			Bus::className(),
			['id' => 'bus_id']
		)->viaTable(
			'{{%driver_bus}}',
			['driver_id' => 'id']
		);
	}

	/**
	 * Получить строку с автобусами водителя через запятую (для заполнения поля в GridView)
	 *
	 * @return string
	 */
	public function getBus_list_string()
	{
		$busModels = Bus::listAsArray($this->buses_ids);
		return implode(', ', $busModels);
	}

	/**
	 * Возвращает самый быстрый автобус данного водителя
	 *
	 * @return Bus
	 */
	public function getFastest() : Bus
	{
		$bus = Bus::find()->filterWhere(['id' => $this->buses_ids])->fastest();
		return $bus;
	}

	/**
	 * Возвращает количество дней для указанной дистанции в километрах
	 *
	 * @param int $distance Дистанция
	 *
	 * @return int Идентификатор найденного автобуса или 0
	 */
	public function getMinDaysByDistance( $distance) : int
	{
		$days = 0;
		$fastest = $this->fastest;
		if ($fastest && $fastest->avg_speed > 0) {
			$days = (int) ($distance / ($fastest->avg_speed * Driver::MAX_WORK_HOURS) );
		}
		return $days;
	}

	/**
	 * Загрузка параметров из формы. В отличии от родительской, сохраняет выбранный файл и привязывает его к модели
	 *
	 * @param array $data Данные для загрузки в форму
	 * @param null  $formName Имя формы
	 *
	 * @return bool Загружена картинка или нет
	 */
	public function load( $data, $formName = null)
	{
		$result = parent::load($data, $formName);
		if ($result) {
			$image = UploadedFile::getInstance($this, 'photo');
			if ($image) {
				if (file_exists(\Yii::getAlias('@webroot').'/upload/drivers/'.$image->name)) {
					$chunks = explode('/', $image->tempName);
					$name = $chunks[count($chunks)-1];
					$chunks = explode('.', $image->name);
					$name .= '.'.$chunks[count($chunks)-1];
					$image->name = $name;
				}
				Imagick::open($image->tempName)
				       ->resize(200, false)
				       ->saveTo(\Yii::getAlias('@webroot').'/upload/drivers/' . $image->name);
				$this->photo = '/upload/drivers/' . $image->name;
			} else {
				$this->photo = $this->getOldAttribute('photo');
			}
		}
		return $result;
	}
}
