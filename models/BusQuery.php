<?php
/**
 * Created by PhpStorm.
 * User: Siniachevsky Constantine
 * Date: 05.06.17
 * Time: 3:04
 */

namespace app\models;


use yii\db\ActiveQuery;


/**
 * Класс BusQuery с условием отбора самого быстрого из списка
 *
 * @package app\models
 */
class BusQuery extends ActiveQuery
{

	/**
	 * Возвращает самый быстрый автобус (по средней скорости) из списка
	 *
	 * @return Bus загруженная модель Автобус (Bus)
	 */
	public function fastest() : Bus
	{
		return $this->addOrderBy(['avg_speed' => SORT_DESC])->limit(1)->one();
	}

	/**
	 * Возвращает один объект класса Bus
	 *
	 * @param null $db
	 *
	 * @return array|null|Bus возвращаемый объект
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}