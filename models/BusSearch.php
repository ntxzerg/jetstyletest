<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bus;

/**
 * BusSearch представляет модель для формы поиска модели Автобус (`app\models\Bus`).
 */
class BusSearch extends Bus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'avg_speed'], 'integer'],
            [['name', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Возвращает набор данных с применением параметров поиска
     *
     * @param array $params Параметры поиска
     *
     * @return ActiveDataProvider Набор данных
     */
    public function search($params)
    {
        $query = Bus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'avg_speed' => $this->avg_speed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
