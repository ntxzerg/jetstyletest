<?php

namespace app\traits;

use yii\web\Response;

/**
 * Trait TraitResult примесь - результат выполнения операции
 * @package app\traits
 */
trait TraitResult
{
	/**
	 * @var bool результат
	 */
	private $success = false;
	/**
	 * @var string текст ошибки или результата
	 */
	private $message = 'Неизвестный результат';
	/**
	 * @var array данные для возвращения результата
	 */
	private $data;

	/**
	 * Установка результата операции
	 *
	 * @param bool $success
	 */
	private function setSuccess(bool $success)
	{
		$this->success = $success;
		$this->message = 'Ok';
	}

	/**
	 * Установка текста результата
	 *
	 * @param string $message
	 */
	private function setErrorMessage(string $message)
	{
		$this->message = $message;
	}

	/**
	 * Добавление пользовательских данных к результату
	 *
	 * @param mixed $data
	 */
	private function setData($data)
	{
		$this->data = $data;
	}

	/**
	 * Получение результата в нужном формате для возврата
	 *
	 * @return array
	 */
	private function getResult() : array
	{
		\Yii::$app->response->format = Response::FORMAT_JSON;
		return [
			'success' => $this->success,
		    'message' => $this->message,
		    'data' => $this->data
		];
	}
}