<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'KPmAR2c_5sIgQ3O46XcT5Q70rzV3_IVj',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        // Менеджер url со включенными pretty-ссылками и разбором ссылок для главной страницы
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            	[
                    'pattern' => 'site/index/<travelFrom:[\w \-]+>/<travelTo:[\w \-]+>',
                    'route' => 'site/index',
                ],
            ],
        ],
        // Менеджер авторизации (простой вариант rbac)
        'authManager' => [
	        'class' => 'yii\rbac\PhpManager',
	        'itemFile' => '@app/rbac/items.php',
	        'ruleFile' => '@app/rbac/rules.php',
	        'assignmentFile' => '@app/rbac/assignments.php',
	        'defaultRoles' => ['admin', 'editor'],
        ],
        // Параметры форматтера для корректного вывода даты рождения
        'formatter' => [
	        'dateFormat' => 'php:Y-m-d',
	        'datetimeFormat' => 'php:Y-m-d H:i:s',
	        'decimalSeparator' => ',',
	        'thousandSeparator' => ' ',
	        'currencyCode' => 'RUR',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
