<?php

return [
    'adminEmail' => 'ntxine@gmail.com',

    // Количество записей на странице, выбрано так, что и 3х2, и 2х3, и 1х6 выглядят нормально
    'pagerPageSize' => 6,

    // url goodle-сервиса DistanceMatrix для получения расстояния между городами
    'googleApi_DistanceMatrix' => 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial',

    // Ключи для доступа к сервисам Google API
    'googleApi_Key' => 'AIzaSyDKmOLEbiM4xy_dLdbEi9oI-KudlpaVghQ',
];
