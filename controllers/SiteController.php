<?php

namespace app\controllers;

use app\models\Bus;
use app\models\Driver;
use app\models\DriverSearch;
use app\models\TravelForm;
use Faker\Factory;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\ForbiddenHttpException;

/**
 * Контроллер сайта (основные страницы)
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Отображение Главной страницы и обработка формы "Поездки".
     *
     * @param string $travelFrom Начальная точка
     * @param string $travelTo Конечная точка
     *
     * @return string
     */
    public function actionIndex($travelFrom = null, $travelTo = null)
    {
	    /** @var \app\models\TravelForm $travel */
	    $travel = new TravelForm();

	    if( $travel->load(Yii::$app->request->post()) &&
	        (($travel->travelFrom != $travelFrom) || ($travel->travelTo != $travelTo))) {

            $request = Yii::$app->request->get();
            unset($request['travelTo']);
            unset($request['travelFrom']);
            $path = array_merge(
			    ['/site/index/'.$travel->travelFrom.'/'.$travel->travelTo],
			    $request
		    );
		    $this->redirect($path);
	    } elseif ($travelFrom && $travelTo) {
		    $travel->travelFrom = $travelFrom;
		    $travel->travelTo = $travelTo;
		    $travel->calculateDistance();
	    }

	    $searchModel = new DriverSearch();
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	    return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'canAdd' => Yii::$app->user->can('update'),
		    'canEdit' => Yii::$app->user->can('update'),
		    'canDelete' => Yii::$app->user->can('delete'),
	        'travel' => $travel,
	    ]);
    }

    /**
     * Вход зарегистрированного пользователя.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Выходит из учётной записи зарегистрированного пользователя.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Отображает страницу Контакты.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

	/**
	 * Генерирует count моделей Bus и Driver посредством Faker
	 * Имена могут быть сгенерированы не в порядке ФИО, нужно проверять
	 * Наименования автобусов генерируются как domainWord, рекомендуется потом менять на адекватные
	 *
	 * @param int $count количество моделей для генерации
	 *
	 * @return string
	 *
	 * @throws ForbiddenHttpException Если действие запрещено
	 */
	public function actionGenerate($count = 1)
	{
		if (!Yii::$app->user->can('update')) {
			throw new ForbiddenHttpException();
		}
		$faker = Factory::create('ru_RU');

		for ($i=0; $i<$count; $i++) {
			$bus = new Bus();
			$bus->name = $faker->domainWord;
			$bus->avg_speed = $faker->numberBetween(30, 90);
			$bus->save();

			$driver = new Driver();
			$driver->name = $faker->name('male');
			$driver->birth_date = $faker->dateTimeBetween('-60 years', '-18 years')->format('Y-m-d H:i:s');
			$driver->active = true;
			$driver->photo = '';
			$driver->save();
		}

		return "Сгенерировано $count автобусов и $count водителей";
    }
}
