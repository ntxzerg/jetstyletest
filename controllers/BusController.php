<?php

namespace app\controllers;

use Yii;
use app\models\Bus;
use app\models\BusSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * BusController реализует CRUD для модели Bus (Автобусы).
 */
class BusController extends Controller
{

    /**
     * Выводит список всех объектов модели.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canAdd' => Yii::$app->user->can('update'),
            'canEdit' => Yii::$app->user->can('update'),
            'canDelete' => Yii::$app->user->can('delete'),
        ]);
    }

    /**
     * Отображает одну запись модели.
     *
     * @param integer $id идентификатор автобуса
     *
     * @return mixed
     *
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
	    return $this->render('view', [
            'model' => $this->findModel($id),
            'canAdd' => Yii::$app->user->can('update'),
            'canEdit' => Yii::$app->user->can('update'),
            'canDelete' => Yii::$app->user->can('delete'),
	    ]);
    }

    /**
     * Создаёт новую запись модели Bus.
     * При удачном создании происходит редирект на view.
     *
     * @return mixed
     *
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
	    if (!Yii::$app->user->can('update')) {
		    throw new ForbiddenHttpException();
	    }

        $model = new Bus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Обновляет существующую модель Bus.
     * При удачном обновлении происходит редирект на view.
     *
     * @param integer $id идентификатор модели
     *
     * @return mixed
     *
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
	    if (!Yii::$app->user->can('update')) {
		    throw new ForbiddenHttpException();
	    }

	    $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Удаляет существующую модель.
     * При удачном удалении редирект на index.
     *
     * @param integer $id идентификатор модели
     *
     * @return mixed
     *
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
	    if (!Yii::$app->user->can('delete')) {
		    throw new ForbiddenHttpException();
	    }

	     $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Ищет модель по первичному ключу
     * Если модель не найдена - выкидывает исключение 404.

     * @param integer $id первичный ключ
     *
     * @return Bus модель

     * @throws NotFoundHttpException Если модель не найдена
     */
    protected function findModel($id)
    {
        if (($model = Bus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
