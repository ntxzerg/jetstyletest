<?php

namespace app\controllers;

use app\traits\TraitResult;
use Yii;
use app\models\Driver;
use app\models\DriverSearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * DriverController реализцет CRUD для модели Driver (водителей).
 */
class DriverController extends Controller
{
	/**
	 * Выделенная логика формирования результата ajax-запроса
	 */
	use TraitResult;

    /**
     * Отображает список водителей.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriverSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canAdd' => Yii::$app->user->can('update'),
            'canEdit' => Yii::$app->user->can('update'),
            'canDelete' => Yii::$app->user->can('delete'),
        ]);
    }

    /**
     * Отображает карточку определённого водителя.
     *
     * @param integer $id идентификатор водителя
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'canEdit' => Yii::$app->user->can('update'),
            'canDelete' => Yii::$app->user->can('delete'),
        ]);
    }

    /**
     * Создаёт новую запись водителя.
     * После успешного обновления переходит на страницу просмотра записи.
     *
     * @return mixed
     *
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
	    if (!Yii::$app->user->can('update')) {
		    throw new ForbiddenHttpException();
	    }
        $model = new Driver();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Обновляет определённую запись водителя.
     * После успешного обновления переходит на страницу просмотра записи.
     *
     * @param integer $id идентификатор модели Driver (водитель)
     *
     * @return mixed
     *
     * @throws ForbiddenHttpException Если нет прав на редактирование записей
     */
    public function actionUpdate($id)
    {
	    if (!Yii::$app->user->can('update')) {
		    throw new ForbiddenHttpException();
	    }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view','id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Отображает определённую модель Driver.
     * После успешного удаления переходит на страницу списка водителей
     *
     * @param integer $id идентификатор удаляемого водителя
     *
     * @return mixed
     *
     * @throws ForbiddenHttpException Если нет прав на удаление записей
     */
    public function actionDelete($id)
    {
	    if (!Yii::$app->user->can('delete')) {
		    throw new ForbiddenHttpException();
	    }
	    $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	/**
	 * Активирует/деактивирует водителя (ajax only)
	 *
	 * @param null $id идентификатор модели водителя (Driver)
	 *
	 * @return array результат выполнения операции в формате json
	 *
	 * @throws BadRequestHttpException если не ajax-запрос
	 */
	public function actionToggleActivity( $id = null)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		if ($id === null) {
			$this->setErrorMessage('Водитель не указан');
		} else {
			$driver = Driver::findOne($id);

			if (!$driver) {
				$this->setErrorMessage('Водитель не найден');
			} else {
				$driver->active = $driver->active ? 0 : 1;

				if (!$driver->save()) {
					$this->setErrorMessage('Ошибка сохранения БД');
				} else {
					$this->setSuccess(true);
					$this->setErrorMessage('Ok');
					$this->setData([
						'id' => $driver->id,
					    'active' => $driver->active,
	                ]);
				}
			}
		}

		return $this->getResult();
    }

    /**
     * Поиск модели по идентификатору
     * Если модель не найдена - выбрасывается исключение 404.
     *
     * @param integer $id Идентификатор модели
     *
     * @return Driver загруженная модель
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driver::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }
}
